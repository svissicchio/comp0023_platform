#!/bin/bash

# stop logging BGP routing tables
for r in $(sudo docker ps | egrep "router$" |  awk '{print $NF}'); do
	proc=$(sudo docker exec -it $r bash -c 'ps aux | grep "looking_glass.sh" | egrep -v "grep"')
        pid=$(echo $proc | awk '{print $2}')
	if [[ ! -z "$pid" ]]; then
		sudo docker exec -it $r bash -c "kill $pid"
	fi
done

