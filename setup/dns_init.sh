#!/bin/bash
#
# sets up DNS root and adjusts hosts so that they can run additional name servers

set -o errexit
set -o pipefail
set -o nounset

curr_dir="$(pwd)/setup"				# this script is called by ../startup.sh

rootns_id="x.univ-root."
rootns_container="1_R2host"
rootns_ip="1.102.0.1"
rootns_script="root-ns.py"
dnslib_source="dnslib-0.9.14.tar.gz"

# install dnslib and tracedns on all hosts
num_hosts=$(sudo docker ps | egrep "host$" | wc -l)
for i in $(seq 1 $num_hosts); do
	container="1_R${i}host"
	sudo docker cp $curr_dir/$dnslib_source $container:/home/
	sudo docker exec $container bash -c "cd /home && tar xzf $dnslib_source && cd $(echo $dnslib_source | sed 's/.tar.gz//g') && python3 setup.py install >/dev/null 2>&1"
	sudo docker exec $container bash -c "rm -Rf home/$(echo $dnslib_source | sed 's/.tar.gz//g')*"
	sudo docker cp ${curr_dir}/tracedns_cy.so ${container}:/home
	sudo docker cp ${curr_dir}/tracedns.py ${container}:/home
done

# launch root NS
sudo docker cp $(readlink -f $curr_dir/$rootns_script) $rootns_container:/home/
sudo docker exec $rootns_container bash -c "cd /home && python3 $rootns_script -a $rootns_ip &"

