#! /bin/bash

currdir=$(pwd)
installdir=$(dirname "${BASH_SOURCE[0]}")

if [[ $# -ne 1 ]]; then
	echo "Usage: $0 <student_number>"
	exit
fi
id=$1

cd $installdir
archive=lab_$1.tar.gz
wget http://www0.cs.ucl.ac.uk/staff/S.Vissicchio/comp0023/dns-cw/$archive

tar xzf $archive -C $installdir

labdir=$installdir/$(ls $installdir | grep "lab_" | egrep -v "\.tar\.gz" | sed 's/\.tar.*//g')
echo $labdir

for f in $(ls $labdir/setup/); do
	if [[ $f  == "startup.sh" ]]; then
		cp $labdir/setup/$f $installdir
	else
		cp $labdir/setup/$f $installdir/setup/
	fi
done

cp $labdir/config/*.txt $installdir/config/

cp -R $labdir/stage1 $installdir
cp -R $labdir/stage2 $installdir

rm -Rf $labdir
rm $archive

cd $currdir

